# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>&lt;web site&gt;/assets/foto/icone/</code>. If your site is <code>http://www.example.com</code>, you should be able to access a file named <code>http://www.example.com/assets/foto/icone/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/foto/icone/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/foto/icone/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/foto/icone/favicon-16x16.png">
    <link rel="manifest" href="/assets/foto/icone/site.webmanifest">
    <link rel="mask-icon" href="/assets/foto/icone/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/assets/foto/icone/favicon.ico">
    <meta name="msapplication-TileColor" content="#4f46e5">
    <meta name="msapplication-config" content="/assets/foto/icone/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)